
# gpg parece emplear el canal de errores estándar para dar información extra (meta-información)
# y el de salida estandar para devolver cosas (los mensajes al descifrarlos, por ejemplo)

import subprocess as sp

def list_keys():
    p = sp.Popen(['gpg', '--list-keys'], stdin=sp.PIPE, stdout=sp.PIPE, stderr=sp.PIPE)
    out, err = p.communicate()
    return out.decode()

"""
Por la salida estándar saca el mensaje descifrado.

Por la salida de errores saca la meta-información con las claves que ha sido cifrado, por ejemplo:
En este caso se han usado dos claves:
gpg: encrypted with 3072-bit RSA key, ID XXXXXXXXXXXXXXXX, created YYYY-MM-DD
      "XXXX <mail@mail.ehu.eus>"
gpg: encrypted with 3072-bit RSA key, ID YYYYYYYYYYYYYYYY, created YYYY-MM-DD
      "YYYY <mail2@mail2.com>"
"""
def desencriptar(mensaje_cifrado):
    p = sp.Popen(['gpg', '-d'], stdin=sp.PIPE, stdout=sp.PIPE, stderr=sp.PIPE)
    out, err =  p.communicate(input=mensaje_cifrado.encode())
    return out.decode()

"""
La salida hace output por la salida de errores (por algún motivo)

Si la clave no está importada:
gpg: key XXXXXXXXXXXXXXXXX: public key "XXXXX <yyyy@zzzzz.kkk>" imported
gpg: Total number processed: 1
gpg:               imported: 1

Si la clave ya existe:
gpg: key XXXXXXXXXXXXXXXXX: "XXXXX <yyyy@zzzzz.kkk>" not changed
gpg: Total number processed: 1
gpg:              unchanged: 1
"""
def importar_clave(clave):
    p = sp.Popen(['gpg', '--import'], stdin=sp.PIPE, stdout=sp.PIPE, stderr=sp.PIPE)
    out, err = p.communicate(input=clave.encode()) #solo devuelve por canal de errores estandar
    return err.decode()

def encriptar(mensaje, recipients):
    cmd = ['gpg', '-a', '--trust-model', 'always', '--encrypt']
    for recipient in recipients:
        cmd.append('--recipient')
        cmd.append(recipient)
    # print('cmd: ')
    # print(" ".join(cmd))
    p = sp.Popen(cmd, stdin=sp.PIPE, stdout=sp.PIPE, stderr=sp.PIPE)
    out, err = p.communicate(input=mensaje.encode())
    return out.decode()

def get_clave_publica(fingerprint):
    cmd = ['gpg', '-a', '--export', fingerprint]
    p = sp.Popen(cmd, stdin=sp.PIPE, stdout=sp.PIPE, stderr=sp.PIPE)
    out, err = p.communicate()
    return out.decode()

