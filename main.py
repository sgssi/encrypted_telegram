#!/usr/bin/env python3

import subprocess as sp
import threading
import time
import gpg
from colorama import Fore, Back, Style
import sys,termios, os, re

PGP_MSG_BEGIN = "-----BEGIN PGP MESSAGE-----"
PGP_MSG_END = "-----END PGP MESSAGE-----"
PGP_BLOCK_START = "-----BEGIN PGP PUBLIC KEY BLOCK-----"
PGP_BLOCK_END = "-----END PGP PUBLIC KEY BLOCK-----"

BACKSPACE_CHAR = "\x7f"

FG_PLAINTEXT = Fore.RESET
FG_DECRYPTED = Fore.GREEN
FG_PROMPT = Fore.YELLOW
FG_PUBLIC_KEY = Fore.MAGENTA
FG_META = Fore.YELLOW
FG_ERROR = Fore.RED


# sino se pone en una lista da error, no guarda globalmente
# [0] = chats, [1] = tipos de chat
lista_de_chats = [[],[]]
# [0] = chat actual, [1] = tipo de chat
chat = ["",""]

is_group_chat=False
history = 5
PROMPT_PREFFIX = [FG_PROMPT +"["+chat[0]+"]:"+ Style.RESET_ALL]# sin [] da error, no guarda globalmente

p = sp.Popen(['telegram-cli', '-C','--wait-dialog-list',
        '--disable-link-preview',
        '-l', '0',
        '--disable-readline'], stdin=sp.PIPE, stdout=sp.PIPE)
pout = p.stdout
pin = p.stdin

listado_claves = {}
global line_buffer
line_buffer = ""

def mensaje_inicial():
    #prototipo de mensaje inicial, hay que revisarlo
    my_print("\n--------------------------------------------------------------------------------------------------------------------\n", FG_META)
    my_print("Bienvenido a nuestra aplicación. Aquí podrás encriptar tus mensajes para aumentar la seguridad de la comunicación.\n", FG_META)
    my_print(" Para la encriptación de la comunicación deberás disponer de claves privada y pública.", FG_META)
    my_print(" También, necesitaras las claves públicas de tus compañeros para poder encriptar los mensajes para ellos.", FG_META)
    my_print("\n - \"/seleccionar\" para seleccionar para quien encriptar los mensajes, de tu listado de claves públicas guardadas.", FG_META)
    my_print(" - \"/help\" para conocer el resto de comandos.", FG_META)
    my_print("\nColores: ", FG_META)
    my_print(" - {}Morado{}: Clave pública recibida compartida por alguien.".format(FG_PUBLIC_KEY, FG_META), FG_META)
    my_print(" - {}Verde{}: Mensajes encriptados. Si no has sido capaz de desencriptarlo apareceran como (secreto).".format(FG_DECRYPTED, FG_META), FG_META)
    my_print(" - {}Blanco{}: Mensajes que no hayan sido encriptados.".format(FG_PLAINTEXT, FG_META), FG_META)
    my_print(" - Amarillo: Otros tipos de mensajes.", FG_META)
    my_print("\n¡Disfruta de nuestra aplicación!.", FG_META)
    my_print("\n--------------------------------------------------------------------------------------------------------------------\n", FG_META)

"""
Habilita/deshabilita el modo echo e icannon del terminal
"""
def enable_echo(fd, enabled, vtime = 0, vmin = 1):
    (iflag, oflag, cflag, lflag, ispeed, ospeed, cc) \
        = termios.tcgetattr(fd)


    if enabled:
        #cc[termios.VTIME] = 0
        #cc[termios.VMIN] = 1
        lflag |= (termios.ECHO | termios.ICANON)
    else:
        #cc[termios.VTIME] = vtime
        #cc[termios.VMIN] = vmin
        lflag &= ~(termios.ECHO | termios.ICANON)

    new_attr = [iflag, oflag, cflag, lflag, ispeed, ospeed, cc]
    termios.tcsetattr(fd, termios.TCSAFLUSH, new_attr)
    return (vtime, vmin)

"""
Print Customizado para sacar salida coloreada y imprimir la frase que se está escribiendo
Siempre se debería utilizar esta función en lugar de print() cuando se quiere mostrar algún mensaje en pantalla
"""
def my_print(str, fore=Fore.RESET, back=Back.RESET, flush=False, end='\n'):
    if(flush == False and end == '\n'):
        print("\r", end='')
        print(" "*(len(line_buffer)+ len(PROMPT_PREFFIX[0])) + " ", end='')
        print("\r", end='')
    print(fore + back +str + Style.RESET_ALL, end=end, flush=flush)
    if(flush == False and end == '\n'):
        print(PROMPT_PREFFIX[0],line_buffer, flush=True, end='')

"""
input() customizado para que la frase que se está escribiendo no se pierda cuando se recibe un mensaje
"""
def get_custom_input():
    fd = sys.stdin.fileno()
    global line_buffer
    line_buffer = ""
    while True:
        vtime, vmin  = enable_echo(fd, False)
        ch = sys.stdin.read(1)
        #print(vtime,vmin)
        #exit(1)
        enable_echo(fd, True)

        if ch == "\n":
            print("\r",flush=True,end='')
            break
        elif ch == BACKSPACE_CHAR:
            if(len(line_buffer) > 0):
                print("\b \b",flush=True, end='')
                line_buffer = line_buffer[:-1]
        else:
            line_buffer += ch
        print(ch, flush=True, end="")

    x = line_buffer
    print("\r", end='')
    print(" "*(len(line_buffer)+ len(PROMPT_PREFFIX)) + " ", end='')
    print("\r", end='')
    line_buffer = ""
    return x

"""
Muestra el menú de las claves públicas guardadas y pregunta al usuario cual de ellas quiere (des)seleccionar
"""
def seleccionar():
    list_keys()
    my_print("\nInserta los numeros de las personas a las que deseas enviar: ", fore=FG_META)
    my_print("\nNOTA: \n  'all' - Seleccionar todas ", fore=FG_META)
    my_print("  'none' - Deseleccionar todas ", fore=FG_META)
    my_print("  (custom) - Seleccionarlas directamente, separandolas por comas (EJ:'1,3,5')\n", fore=FG_META)
    seleccion = get_custom_input()
    i = 0

    if seleccion == "all":
        for fingerprint in listado_claves:
            listado_claves[fingerprint][1] = True
    elif seleccion == "none":
        for fingerprint in listado_claves:
            listado_claves[fingerprint][1] = False
    else:
    #elif type(seleccion) == list:

    #faltaría comprobar que si no es all o none que solo sean números

        for fingerprint in listado_claves:
            #valores[0] = email y nombre
            #valores[1] True/False -> la clave está seleccionada
            # Hacemos un "toggle" de los valores introducidos.

            if str(i) in seleccion.split(","):
                listado_claves[fingerprint][1] = not listado_claves[fingerprint][1]
            i += 1

    limpiar_consola() # limpiar la lista del terminal
    list_keys() # imprimir lista actualizada

def seleccionar_chats():
    nuevo_chat_id = -1

    for i in range(len(lista_de_chats[0])):
        my_print("{:^3}- {}".format(i, lista_de_chats[0][i]), fore=FG_META)

    my_print("\n Escribe el numero del chat al que quieres cambiar:", FG_META)
    try:
        nuevo_chat_id = int(get_custom_input())
    except:
        my_print("No es un numero valido.", FG_ERROR)
        return

    limpiar_consola() # limpiar la lista del terminal

    if nuevo_chat_id >= len(lista_de_chats[0]) or nuevo_chat_id < 0:
        my_print("Este chat no existe.", FG_ERROR)
    else:
        #cambiar al chat
        chat[0] = lista_de_chats[0][nuevo_chat_id]
        chat[1] = lista_de_chats[1][nuevo_chat_id]
        PROMPT_PREFFIX[0] = FG_PROMPT +"["+chat[0]+"]:"+ Style.RESET_ALL

def compartir_clave():
    claves = []
    list_keys(checkboxes=False)
    my_print("\n Inserta los numeros de las claves que quieres compartir (separado por comas EJ:1,3,5):", fore=FG_META)
    seleccion = get_custom_input()
    limpiar_consola()
    i = 0
    for fingerprint in listado_claves:
        # valores[0] = email y nombre
        # valores[1] True/False -> la clave está seleccionada
        # Añadimos las claves a la lista de claves a compartir
        if str(i) in seleccion.split(","):
            #print("Añadiendo clave(s) de {}".format(listado_claves[fingerprint][0]))
            claves.append(fingerprint)
        i += 1
    #print(claves)
    for clave in claves:
        clave_publica = gpg.get_clave_publica(clave)
        clave_publica = '\\n'.join(clave_publica.splitlines())
        send_command("msg {} \"{}\"".format(chat[0], clave_publica))


# COMANDO: /listar
# imprime las claves que estan guardadas en el gestor de gpg
def list_keys(checkboxes=True):

    out = gpg.list_keys()
    # Trim lineas sin datos relevantes
    out = out.split("--\n")[1].split("\n\n")
    """
    entry:

    pub   rsa2048 YYYY-MM-DD [SC] [expires: YYYY-MM-DD]
        F815C85E8DC37C1B666F94DE6EF8F400FB730F2D
    uid           [ultimate] XXXXXX <mail@example.com>
    sub   rsa2048 YYYY-MM-DD [E] [expires: YYYY-MM-DD]
    """
    for entry in out:
        lines = entry.split("\n")
        if len(lines) == 4:
            fingerprint = lines[1].strip()
            owner = lines[2].strip().split("]")[1]
            # Si no tenemos la clave guardada, la añadimos a la lista
            if fingerprint not in listado_claves:
                listado_claves[fingerprint] = [owner, False]
    i = 0
    for fingerprint , valores in listado_claves.items():
        # valores[0] = email y nombre
        #valores[1] True/False -> la clave está seleccionada
        #Imprimimos los valores.
        if checkboxes:
            if valores[1]:
                my_print(str(i) + "-[X]"+ valores[0] + ": " + fingerprint, fore=FG_META)
            else:
                my_print(str(i) + "-[ ]"+ valores[0] + ": " + fingerprint, fore=FG_META)
            i+=1
        else:
             my_print(str(i) + "-" + valores[0] + ": " + fingerprint, fore=FG_META)
             i+=1

# se lee el bloque linea a linea para luego desencriptarlo
def leer_bloque(line):
    raw = line
    while threads_running:
        line = pout.readline().decode().strip()
        raw = raw + "\n" + line
        if PGP_MSG_END in line:
            mensaje_desencriptado = gpg.desencriptar(raw)
            return mensaje_desencriptado

"""
Return: Tupla de [autor, añadido(True)/ya existente(False)]
"""
def leer_bloque_clave_publica(mensaje):
    while threads_running:
        line = pout.readline().decode().strip()
        mensaje = mensaje + "\n" + line
        if PGP_BLOCK_END in line:
            clave_publica_nueva = gpg.importar_clave(mensaje)
            # print("Se ha añadido una nueva clave pública:")
            # Parsear
            firstline = clave_publica_nueva.splitlines()[0]
            _, identificador, state = firstline.split("\"")
            state = state.strip()
            if(state == "not changed"):
                return [identificador, False]
            else:
                return [identificador, True]

# FALTA MIRAR LO DE SEPARAR TODO BIEN (HORA, GRUPO, ETC)
def decompose_line(line):
    """
    Regex que descompone la estructura del mensaje. Los grupos son:
    0- [fecha]
    1- [GOUP:grupo] o None si es chat entre 2 personas
    2- grupo o None si es chat entre 2 personas
    3- sender
    4- separador
    5- mensaje
    """
    regex = "^(\\[.+?\\])\\s+(\\[.+:(.+)\\])?\\s*(.*)\\s+([<>»]{3})\\s+(.*)$"
    x = re.search(regex, line)
    if x is None:
        return [None, None, None, None, None, None]
    else:
        return x.groups()


"""
Trata la línea que se ha recibido desde telegram-cli
- Si es un mensaje en texto plano, lo imprime
- Si es un mensaje de clave pública, la importa e imprime un mensaje
- Si es un mensaje cifrado, lo descifra y lo imprime
- Si es un mensaje de dialog_list guarda el chat (excepto los canales) en lista_de_chats
"""
def parse_line(line):
    #autor, separator, cortado = line.partition(" »»» ") #problema con separator, a veces es '>>>'
    ##autor, separator, cortado = decompose_line(line)
    date, _, group, sender, sep, msg = decompose_line(line)

    if group is not None:
        group = group.replace(" ", "_")

    if chat[1] == "Chat" and (group is None or group != chat[0]):
        # El mensaje entrante no corresponde al chat seleccionado
        #print('Filtrado')
        return
    if chat[1] == "User" and sender != chat[0] and group is not None:
        # El mensaje entrante no corresponde al chat seleccionado
        #print('Filtrado')
        return

    if date is None:
        prefix = "" + line
        msg = line
    else:
        sep = ":"
        if group is None:
            prefix = "{} {}{} ".format(date, sender, sep)
        else:
            prefix = "{} [{}] {}{} ".format(date, group, sender, sep)

    # se comprueba si es un mensaje encriptado, si lo es, se intentara desencriptarlo

    if PGP_MSG_BEGIN in msg and date is not None:
        mensaje = leer_bloque(msg)
        if mensaje != "":
            my_print(prefix + mensaje, fore=FG_DECRYPTED)
        else:
            my_print(prefix +  "(secreto)", fore=FG_DECRYPTED)
    elif PGP_BLOCK_START in msg and date is not None:
        clave, estado = leer_bloque_clave_publica(msg)
        my_print(prefix + "Ha compartido la clave pública de (" + clave + ")", fore=FG_PUBLIC_KEY)

        if(estado): # preguntar si se quiere guardar la clave??
            my_print("Se ha importado la clave de " + clave, fore=FG_META)

    # Si empieza por ">" es un mensaje interno de telegram-cli
    elif line.startswith(">"):
        # Borrar basura que genera telegram-cli
        line = line.split("\r")[-1].strip("\x1b[K").strip()
        if line.startswith("["):
            # Es un caso especial, hay que volver a parsearlo
            parse_line(line)
        else:
            # Se descarta la linea porque no es relevante
            pass

    # Si es uno de los chats que tienes (listados por dialog_list)
    # linea de ejemplo --> "Channel Muy Computer: 1 unread"
    elif msg.split(" ")[0] in ["User", "Channel", "Chat"]:
        # NOTA: Los bots salen tambien como usuarios.

        # No se puede escribir a chanales por lo que se ignoraran
        if msg.split(" ")[0] != "Channel":
            # chatname es lo que haya hasta los dos puntos (sin incluir los dos puntos),
            # quitando lo que haya hasta el primer espacio (inclusive el espacio)
            tipo_chat = msg.split(" ")[0]
            chatname = "_".join(msg.split(":")[0].split(" ")[1:])

            lista_de_chats[0].append(chatname)
            lista_de_chats[1].append(tipo_chat)


    # si no lo es imprime directamente el mensaje tal cual
    else:
        if date is not None:
            my_print("\r{}{}".format(prefix, msg))
        #else: # metadatos del telegram_cli
            #my_print("\r" + line,FG_ERROR)


# EJEMPLO DE MENSAJE:
#    [05 Dec] SGSSI Mungs »»» bien bien
# main de leer
def thr_parse():
    global line_buffer
    while threads_running:
        line = pout.readline().decode().strip()
        parse_line(line)

"""
Envía un comando por telegram_cli
"""
def send_command(command):
    command = command + "\n"
    pin.write(command.encode())
    pin.flush()

"""
Devuelve una lista con todos los recipientes seleccionados
"""
def get_active_recipients():
    activos = []
    for fingerprint, valor in listado_claves.items():
        if valor[1]:
            activos.append(fingerprint)
    return activos

def limpiar_consola():
    os.system('cls' if os.name == 'nt' else 'clear')

def historico(n = history):
    send_command("history {} {}".format(chat[0], n))


# main de enviar
# se añade time.sleep(1) con cada comando para asegurar que no de problemas
# se carga el dialog_list para que se carguen los usuarios y grupos, si no se cargan puede dar problemas al intentar mandar mensajes
def thr_input():
    hist_regex = "^/historico ([0-9]+)$"

    time.sleep(1)
    send_command("dialog_list") # muestra una lista actualizada de los chats, se trata en parse_line()
    time.sleep(1) # para que le de tiempo a ver (y guardar en lista_de_chats) que chats existen
    while not chat[0]:
        seleccionar_chats()
    seleccionar()
    historico()

    while threads_running:

        x = get_custom_input()

        if x == "/listar":
            #limpiar_consola()
            my_print("Listando claves", fore=FG_META)
            list_keys()
            #historico(history)

        elif x == "/historico":
            limpiar_consola()
            historico()
        elif re.match(hist_regex, x):
            n = int(re.match(hist_regex,x).groups()[0])
            limpiar_consola()
            historico(n)
        elif x == "/salir":
            #salir de la aplicacion
            send_command("quit")
            my_print("\nAdios amigo\n", FG_META)

        elif x == "/chat":
            seleccionar_chats()
            historico() # con el chat actualizado

        elif x == "/seleccionar":
            limpiar_consola()
            my_print("Seleccionando claves", fore=FG_META)
            seleccionar()
            historico()

        elif x == "/compartir":
            my_print("\nSelecciona que claves quieres compartir por el chat:", fore=FG_META)
            compartir_clave()

        elif x == "/help":
            my_print("\nNECESITAS AYUDA?", fore=FG_META)
            my_print("/help -> Muestra la ayuda", fore=FG_META)
            my_print("/listar -> Listar todos los recipientes", fore=FG_META)
            my_print("/seleccionar -> Seleccionar recipientes", fore=FG_META)
            my_print("/compartir -> Compartir clave pública", fore=FG_META)
            my_print("/historico -> Muestra los últimos 5 mensajes del chat", fore=FG_META)
            my_print("/historico n -> Muestra los últimos n mensajes del chat", fore=FG_META)
            my_print("/salir -> Salir del programa", fore=FG_META)
            my_print("/chat -> Seleccionar chat", fore=FG_META)

        elif x.startswith("/"):
            my_print("\nEse comando no existe", fore=FG_ERROR)
        else:
            # Si no es un comando, es un mensaje que se envia al chat @chat
            recipients = get_active_recipients()
            if(len(recipients) == 0):
                my_print("Error, debes seleccionar los recipientes. Escribe /help para ver la ayuda", fore=FG_ERROR)
            else:
                x = gpg.encriptar(x, recipients)
                x = '\\n'.join(x.splitlines())
                send_command("msg {} \"{}\"".format(chat[0], x))
                time.sleep(1)

threads_running = True
mensaje_inicial()
#Thread de leer telegram-cli
th1 = threading.Thread(target=thr_parse, daemon=True)
th1.start()
# Thread de enviar telegram-cli
th2 = threading.Thread(target=thr_input, daemon=True)
th2.start()

while(p.poll() == None):
    time.sleep(1)
threads_running = False
